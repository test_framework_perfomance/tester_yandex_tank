#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from functools import lru_cache


import pathlib
import random
from typing import Any

import orjson


class GeneratorAmmo:
    HOST_PORT: str = 'localhost:8000'
    API_KEY: str | None = None
    URI: str = '/api/v1/test_data'
    TAG: str = 'test_django'
    PROPORTION_POST_GET: list[int] = None
    _body_list: list[str]
    _base: pathlib.Path = None
    _get_index: int = 0

    def __init__(self, count_columns_in_json: int, proportion_post_get: list[int] = [1, 5]) -> None:
        """Класс предназначен для генерации файла запросов для yandex tank ammo.json

        Args:
            count_columns_in_json (int): какое количество колонок в json должно быть, файлы с соответствующими json должны быть заранее сформированы
            proportion_post_get (list[int], optional): Определяет соотношение запросов post и get. Defaults to [1, 5]. При дефолтной настройке на 1 post запрос будет 5 get запросов
        """
        self._base = pathlib.Path(__file__).parent
        self._read_body(count_columns_in_json=count_columns_in_json)
        self.PROPORTION_POST_GET = proportion_post_get

    def _read_body(self, count_columns_in_json: int) -> None:
        self._body_list = []
        file_name = f'body_post_entity_{count_columns_in_json}_column_in_json.json'
        path_to_file = self._base / file_name
        if not path_to_file.exists():
            raise ValueError(f'File {path_to_file} not found')
        with open(path_to_file, 'r') as file:
            json_string = file.read()
            data = orjson.loads(json_string)
            self._body_list = [{'data': item['data']} for item in data['items']]

    def generate_ammo(self, count_requests: int = 100, batch_size: int = 1000) -> bool:
        """Сгенерировать ammo.json

        Args:
            count_requests (int, optional): Количество запросов. Defaults to 100.
            batch_size (int, optional): размер пачки get запроса. Defaults to 1000.

        Returns:
            bool: _description_
        """
        IS_POST = 0
        IS_GET = 1
        list_choice = [IS_POST, IS_GET]
        count_get = int(count_requests * self.PROPORTION_POST_GET[1] / (self.PROPORTION_POST_GET[0] + self.PROPORTION_POST_GET[1]))
        count_post = count_requests - count_get
        list_request: list[dict[str, Any]] = []
        self._get_index = 0
        while (count_get > 0) or (count_post > 0):
            method = random.choice(list_choice)
            if method == IS_POST and count_post > 0:
                list_request.append(self._get_next_post_request())
                count_post -= 1
            elif method == IS_GET and count_get > 0:
                list_request.append(self._get_next_get_request(batch_size=batch_size))
                count_get -= 1

        self._save_ammo(list_request=list_request)


    def _get_next_post_request(self) -> dict[str, Any]:
        count_items = random.randint(1, 2)
        list_data = []
        for _ in range(count_items):
            list_data.append(random.choice(self._body_list))
        body = orjson.dumps({'data_input': list_data}).decode('utf-8')
        return {
            'tag': self.TAG,
            'uri': self.URI,
            'method': 'POST',
            'headers': self._get_headers(),
            'host': self.HOST_PORT,
            'body': body,
        }

    def _get_next_get_request(self, batch_size: int) -> dict[str, Any]:
        url = f'{self.URI}?from_index={self._get_index}&batch_size={batch_size}'
        self._get_index += batch_size
        return {
            'tag': self.TAG,
            'uri': url,
            'method': 'GET',
            'headers': self._get_headers(),
            'host': self.HOST_PORT,
            # 'body': body,
        }

    def _save_ammo(self, list_request: list[dict[str, Any]]) -> None:
        with open(self._base.parent / 'ammo.json', 'w') as f:
            for request in list_request:
                text_data = orjson.dumps(request).decode('utf-8')+'\n'
                f.write(text_data)

    @lru_cache()
    def _get_headers(self) -> dict:
        headers = {
            'Accept': '*/*',
            'User-agent': 'Tank',
            'Connection': 'close',
            'Content-Type': 'application/json',
        }
        if self.API_KEY is not None:
            headers['Authorization'] = f'Api-Key {self.API_KEY}'

        return headers


if __name__ == '__main__':
    proportion_post_get = [1, 5]
    generator = GeneratorAmmo(count_columns_in_json=100, proportion_post_get=proportion_post_get)
    generator.generate_ammo(count_requests=100, batch_size=1000)
